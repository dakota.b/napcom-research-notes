Observational Notes
===================

- Time of operation on Intel I5 4790k/24GB RAM@3000MHz/NVMe SSD
    - 1:30 with optimized HPMS dataset
    - Not tested without optimized HPMS dataset
- Time of operation on business-class data processing machine
    - ? with optimized HPMS dataset
    - ? with raw HPMS dataset
- Files changed during operation
    - ?
    - ?
- Files changed as a conclusion of operation
    - ?
    - ?
- Files to target for memory-based performance improvement
    - ?
- Procedures which can be parallelized
- 
# How NAPCOM Works

The following is a summary of the data flow
throughout the NAPHCAS program as interpreted
by software engineer Dakota Brinson.

## General Quirks

NAPCOM 2008 has some quirks that make it function differently
than the program suggests. Here are the biggest differences:

1. The program says that it asks for manual control file input
   if the file is not present. This isn't true, though: the
   control file information has to be in a control file in
   the syntax that NAPCOM will accept.
2. The program says that it compresses data for rapid processing.
   This is only partially true: code to read the compressed
   section information is commented out, so changing the
   `Type of File` field in the control file has no effect.
3. <span id="rsltprn-quirk">The program exits early due to a crash.
   This is a huge issue for the operation
   of the program, because it doesn't actually get to write
   any of the output files from the subroutine `RSLTPRN`. Additionally,
   NAPCOM only makes it through processing 4 states. On state 5,
   some entry in FC 7 causes the crash. This is
   due to a [math error](#math-error). Research suggests that
   the issue is due to the program taking the **log10 of 0**</span>
4. In addition to the crash described in (3), analysis of the
   program describes a pattern of design which would lead to
   a state of infinite instructional execution. This is because
   in the [Processing Group Function](#processing-group-function),
   the final line of the function is `go to 1`. With a proper
   exit procedure, this behavior would be normal and
   unproblematic. However, [Processing Group Function](#processing-group-function)
   offers no such exit procedure such as `return` or `go to` statements.

<p id="math-error">Math Error</p>

```
run-time error M6202: MATH
- **: SING error
```

## Index of Files

FORTRAN requires that files are opened with a
file reference number, and written to/read from
based on this reference number.

Additionally, all files are opened with the python
mode R/W or `r+`.

1. <span id="file-1">Variable Files:</span>
   1. <span id="file-1-statedat">State-Dependent Design and Environmental Variables (`statedat`)</span>
   2. <span id="file-1-rehabcst">Rehab Cost Data (`rehabcst`)</span>
   3. <span id="file-1-ab">RHOBET coefficients (`ab.out`)</span>
   4. <span id="file-1-rvout">Rigid Pavement Variables (`rvout`)</span>
   5. <span id="file-1-dp">Score and Damage Limit Variables (`dp.dat`)</span>
   6. <span id="file-1-wtvhr">Vehicle Weights File (`wtvhr.dat`)</span>
   7. <span id="file-1-axwts">Axle Weights Table (`axwts.out`)</span>
   8. <span id="file-1-config">Axle Configs Table (`config.dat`)</span>
2. <span id="file-2">hpmsgap</span>
3. <span id="file-3">Standard IO Reporting Files</span>
   1. <span id="file-3-chkout">Checkout IO (`chkout`)</span>
   2. <span id="file-3-nhptabs">NHP Tables (`nhptabs`)</span>
4. <span id="file-4">NHP Traffic study file (`nhptraf`)</span>
5.
6.
7. Control File or Pavement Flex File
   1. <span id="file-7">cntrl.hca</span>
   2. <span id="file-7.2">rpflex.prn</span> - results for flexible pavement
8. <span id="file-8">rprigi.prn</span> - results for rigid pavement
9. Variable based on I or O:
   1. <span id="file-9">9806SRT2.TXT</span> - HPMS data file (2006 format)
   2. <span id="file-9.2">rpashr.prn</span> - results for attributable shares
10. <span id="file-41">2006-24-3.prn</span>
11.
12. <span id="file-23">frshrs.out</span> - Relative shares, flexible pavements
13. <span id="file-24">rrshrs.out</span> - Relative shares, rigid pavements
14. Output files
    1. <span id="file-25">ashrs.out</span> - Shares by distress type.
    2. <span id="file-25.2">lefs.out</span> - Load equivalency factors

---

## Main Function

NAPCOM 2008 Name: `NAPHCAS`

NAPCOM begins by processing its main function:

Uses variables from common group [CN](#group-cn)

1. Initializes `mode` variable as `0`
2. Prints the splash screen
3. Opens [chkout](#file-3-chkout) file
4. Calls [Control File Parsing](#control-file-parsing) function
5. Calls [File Data Import](#file-data-import) function
6. Calls [HPMS Data Preprocessing Routine](#hpms-data-preprocessing-routine) if the control file says it is not NAPCOM-optimized
7. Calls [Processing Group Function](#processing-group-function)
8. Calls [Result Printing Function](#result-printing-function) [[(!) WARNING](#rsltprn-quirk)]

_NAPCOM also has a function for printing results to files, but this is not used because of a bug in PROCGRP causing the program to exit early_

---

## Control File Parsing

Napcom 2008 Name: `CONTROL`

Looks for the file cntrl.hca.
Allows you to use an alternative control file.

_NAPCOM program notes say that you could interactively enter all options, but the functionality is not provided._

Uses variables from common groups [CN](#group-cn) and [C3](#group-c3)

1. Declares local variables `there`, `rsp`, `fnam`
2. Checks of [cntrl.hca](#file-7) exists and opens it or the
   user-defined control file
3. Reads the name of the section data file and opens it if it exists.
4. Reads the line that detects whether the file is NAPCOM Rapid Processing enabled.
5. If the file is NAPCOM-preprocessed, opens [file 2](#file-2)
6. If not, it opens the [section data file](#file-9)
7. Reads the line that contains the file name of the [rapid processing file](#file-2)
   to save NAPCOM rapid processing data to
8. Reads the name of [the Traffic File](#file-41)
9. Reads traffic diff response and saves it as `trdif (boolean)`
10. Reads the analysis period for the study and saves it as `ap (int)`
11. Reads the selected states, functional classes, and pavement types to analyze on three separate lines as space-separated integer arrays.
12. Reads interpolation response and saves it as `ntrp (boolean)`
13. Reads print request and saves it as `prnt (boolean)`
14. Reads record skip amount and saves it to `nskp (int, default 1)`
15. Closes the [control file](#file-7)
16. Reports on what it intends to do both to console and [file 3](#file-3-chkout)
17. Returns to [Main Function](#main-function)

---

## File Data Import

NAPCOM 2008 Name: `READFLS`

Opens input files and read input data.

Uses variables from common groups [SD](#group-sd), [RC](#group-rc), [AB](#group-ab),
[CN](#group-cn), [DP](#group-dp), [RP](#group-rp), and [VM](#group-vm)

1. [Process VMT Data](#procvmt)
2. Open [statedat](#file-1-statedat) and read into [SD](#group-sd), close it.
3. Open [rehabcst](#file-1-rehabcst) and read into [RC](#group-rc), close it.
4. Open [ab.out](#file-1-ab) and read into [AB](#group-ab), close it.
5. Open [rvout](#file-1-rvout) and read into [RP](#group-rp), close it.
6. Open [dp.dat](#file-1-dp) and read into [DP](#group-dp), close it.
7. Return to [Main Function](#main-function)

---

## Process VMT Data

NAPCOM 2008 Name: `PROCVMT`

This subroutine reads vmt, weight, and axle information.

Uses variables from common gorup [VM](#group-vm)

1. declares `hc` and `rg` adjustment tuples
2. Open [wtvhr.dat](#file-1-wtvhr) and read into [VM.wtds](#group-vm), close it.
3. Read from [Traffic File](#file-41) into [VM.vmt](#group-vm)
4. Opens [VMT File](#file-33) and writes [VM.vmt](#group-vm) to it.
5. Closes [Traffic File](#file-41)
6. Opens [axwts.out](#file-1-axwts) and read into [VM.axwts](#group-vm), closes it.
7. Opens [config.dat](#file-1-config) and read into [VM](#group-vm), closes it
8. [Transform axle weights](#axwt-proc)
9. [Calculate total VMT and Truck Percents for each State and FC](#total-vmt-proc)
10. Return to [File Data Import](#file-data-import)

<p id="axwt-proc">Axle Weight Transformation Procedure</p>

```python
def axwt_transform(vm):
    for vc in raange(24):
        for wg in range(40):
            vsum = 0.0
            for h in range(4):
                for r in range(10):
                    vsum += vm.wtds[wg][h][r][vc]
            i = 1
            for ax in range(nax[vc]):
                L2 = vm.axtp[ax][vc]
                Lx = vm.axwts[i][wg][vc]
                i += 1
                # This doesn't make any sense... add 0, reset, i++?
                if L2 >= 2 or L2 == 3:
                    i += 1
                if vsum < 1.0e-11:
                    Lx = 0.0
                vm.axwts[i][wg][vc] = Lx
    return vm
```

<p id="total-vmt-proc">Calculate Total VMT and Truck Percents for each State and FC</p>

```python
def total_vmts(vm):
    for fc in range(12):
        for st in range(51):
            for i in range(3):
                vm.tvm[i][fc][st] = 1.0e-20 # nonzero zero for division
            for vc in range(24):
                vm.tvm[0][fc][st] += vm.vmt[vc][fc][st]
                i = typ[vc]
                if i != 0:
                    vm.tvm[i+1][fc][st] += vm.vmt[vc][fc][st]
            # transform VMT to vsh and calculate Jake array
            vm = vmt_transform(vm, st, fc)
```

<p id="transform-vmt-proc">Transform VMT to VSH and calculate Jake array</p>

```python
def vmt_transform(vm, st, fc):
    for vc in range(24):
        for wg in range(40):
            if st == 0:
                vm.vmtc[wg][vc][fc] = 0.0
                vm.vmcr[wg][vc][fc] = 0.0
            vm.vmtc[wg][vc][fc] += vm.tvm[vm.typ[vc]][fc][st] * \
                vm.vmt[vc][fc][st] * vm.wtds[wg][vm.hc[fc]][vm.rg[st]][vc]
    for i in [1,2]:
        vm.tvm[i][fc][st] /= vm.tvm[0][fc][st]
    return vm
```

---

## HPMS Data Preprocessing Routine

NAPCOM 2008 Name: `HPMSDAT`

Processes the input HPMS data, if necessary,
to put in the NAPCOM speed-enabling format.

Uses variables from common groups [HP](#group-hp), [TB](#group-tb), [CV](#group-cv),
[CN](#group-cn), [AC](#group-ac), and [VM](#group-vm)

1. Calls [HPMS Data Setup](#hpms-data-setup) function
2. Open [nhptabs](#file-3-nhptabs) and [nhptraf.out](#file-4)
3. Open [testread.out](#file-31)
4. Begin loop and read HPMS vars from [9806SRT2](#file-9)
5. Apply [preprocessing transformations](#transformation-hpmsdat) to data
6. fill `fc` and `st` with `fcin` and `stin` run through conversion arrays in [CV](#group-cv)
7. If the FC or State is 99 (code for invalid or not considered), loop back to beginning
8. Corrects truck percentage SU and CMB if they are zero [+](#truck-percentage)
9. Calls subfunctions if the current state (`st`) is different from the last state (`stpr`)
10. Fill [HP](#group-hp) values with [HP Value Conditional](#hp-value-conditional)
11. Tabulate the frequncy of
    occurence of each possible value (or range of values) of selected
    HPMS input variables.
12. Transfer the input and processed variables to the correct positions in the state master array.
13. Set the prior state and loop back to (4) until all records have been read
14. report number of records
15. Call subfunctions one more time
16. Reports total number of records read (weird... why did we report that in (14)?)
17. Reports the total number of records written by functional class
18. Close [nhptabs](#file-3-nhptabs), Open [chkout](#file-3-chkout),
    Close [](#file-4), Close [9806SRT2.txt](#file-9)
19. Returns to [main](#main-function)

<p id="transformation-hpmsdat">Section Data Preprocessing Transformation</p>

```fortran
	 yf = yf - 2000
       psr = psrr * 10. + 0.5
	 If (ptype .ge. 5) sn = snr + 0.5
	 If (ptype .le. 4) sn = snr * 10. + 0.5
      If (psr .lt. 15) then
         If (iri .gt. 0.2) then
            If (ptype .le. 4) psr = 50 * EXP(-0.0038 * iri)
            If (ptype .eq. 5) psr = 50 * EXP(-0.0043 * iri)
            If (ptype .eq. 6) psr = 50 * EXP(-0.0046 * iri)
            If (psr .gt. 45) psr = 45
            If (psr .lt. 15) psr = 15
         Else
            psr = 25
         Endif
      Endif
```

<p id="truck-percentage">Correct truck percentage SU and CMB if they are zero</p>

```fortran
      i = fc
      If (fc .ge. 6) i = fc + 1
      If (su .eq. 0 .and. cmb .eq. 0) then
         su  = 100. * tvm(2,i,st) + 0.5
         cmb = 100. * tvm(3,i,st) + 0.5
      Elseif (su .eq. 0) then
         su  = cmb * tvm(2,i,st) / (tvm(2,i,st)+tvm(3,i,st)) + 0.5
         cmb = cmb - su
      Elseif (cmb .eq. 0) then
         cmb = su  * tvm(3,i,st) / (tvm(2,i,st)+tvm(3,i,st)) + 0.5
         su  = su - cmb
      Endif
        If (cmb .le. 0) cmb = 1
        If (su  .le. 0) su  = 1
        If (cmb .gt. 99) cmb = 99
        If (su  .gt. 99) su  = 99
```

<span id="hp-value-conditional">Sets the values of <a href="#group-hp">HP</a></span>

```fortran
      tm(fc) = tm(fc) + expf * leng
      nr(fc) = nr(fc) + 1
      nrn(fc) = nrn(fc) + 1
      If (adt .ge. 50.) then
         tadt(fc) = tadt(fc) + expf * leng * adt
         ttsm(fc) = ttsm(fc) + expf * leng
         tsu(fc)  = tsu(fc)  + expf * leng * adt * su  * 0.01
         tcm(fc)  = tcm(fc)  + expf * leng * adt * cmb * 0.01
         nyr      = yf - yr
         if (nyr .lt. 1)  nyr = nyr + 100
         if (nyr .gt. 50) nyr = 50
         grrt     = fadt / adt
         if (grrt .lt. 0.1) grrt = 0.1
         grrt     = grrt ** (1. / nyr) - 1.
         if (grrt .lt. -0.12) grrt = -0.12
         if (grrt .gt. +0.12) grrt = +0.12
         tfdt(fc) = tfdt(fc) + adt * grrt ** 20
      Else
         grrt = 0.
      Endif
```

---

## HPMS Data Setup

NAPCOM 2008 Name: `HPSET`

Sets the values used to convert HPMS state and functional
class codes to NAPCOM indexes, initializes the record-counting
array for the first state, and sets the allowable ranges for
selected variables.

Uses variables from common gorup [HP](#group-hp), [TB](#group-tb), [CV](#group-cv), and [AC](#group-ac)

1. Set up State Convert array
2. Set up FC Convert array
3. Zero out the table entries for [Tb](#group-tb) for `n`
4. Enter [HPMS Reset Function](#hpms-reset)

### HPMS Reset

NAPCOM 2008 Name: `RESET`

RESET initializes values at the end of each state's records.

1. Zero the [AC](#group-ac) arrays
2. Zero the [TB](#group-tb) arrays for `s`
3. Returns to [Processing Group](#processing-group-function)

### HPMS Write

NAPCOM 2008 Name: `HPWRIT`

Processes stored HPMS data for a single state, and writes
consolidated records to a new file. Called when [HPMS Data Preprocessing Function](#hpms-data-preprocessing-routine)
encounters a new state in the HPMS file.

For each FC, [HPMS Write](#hpms-write) does the following:

1. Writes state to [file 4](#file-4)
2. Shifts FC to skipe FC "5"
3. Calculate [AC](#group-ac) variables
4. Write [AC](#group-ac) variables to [Rapid Processing Binary File](#file-2)
   - Analyst suggests splitting this write into another binary file so that items of different data dimensions aren't being attempted to be stored together in fixed-dim table
5. Write [AC](#group-ac) variables to [human readable file](#file-4)
6. Establish the number of records left to write, or 1000 max.
7. Write up to 1000 records from [HP](#group-hp) to [Rapid Processing Binary File](#file-2)
8. Write remaining records from [HP](#group-hp), if they exist (?)
   - Analyst believes this may be simply working around some kind of constraint FORTRAN imposes and recommends writing the values using `pandas.DataFrame.save_pickle` method.
9. Return to [HPMS Data Setup](#hpms-data-setup)

### State Tabulation

NAPCOM 2008 Name: `STATAB`

Writes the tables of HPMS data values for a single state from [TB S Series](#group-tb)
to [National Highway and Pavement Tables](#file-3).

1. Writes the formatted table of facility type by functional class
2. Writes the table of ADT range (upper limit)
3. Writes the table of Number of Lanes
4. Writes the table of Pavement Type
5. Writes the Table of Pavement Key
6. Writes the table of Structural Number/Thickness
7. Writes the table of Pavement Serviceability Index (PSR)
8. Writes the table of Year of Improvmenet
9. Writes the table of Lane Width
10. Writes the table of Single Unit Truck Percentages
11. Writes the table of Combination Truck Percentages
12. Writes the table of Single Unit Factors
13. Writes the table of Directional Factors
14. Writes the table of Capacity
15. Writes the table of Forecast Year
16. Writes the table of Climate Zone
17. Writes the table of Drainage Type
18. Writes the table of Growth Rate
19. Returns to [Processing Group](#processing-group-function)

---

## Processing Group Function

NAPCOM 2008 Name: `PROCGRP`

Processing group is the part of NAPCOM that applies the models of the program
to the data. It is the largest and most intensive part of the program with many
calls to other subroutines. It crashes upon reaching State # 5 FC # 7 due to a math
error possibly due to the logarithm taken on line 1579 of the FORTRAN 77 source.

The analyst believes that the

Uses variables from common groups [CN](#group-cn), [C3](#group-c3), [VM](#group-vm),
[DP](#group-dp), [SC](#group-sc), [SH](#group-sh), [SD](#group-sd), [RC](#group-rc),
[P2](#group-p2), [P3](#group-p3), [S2](#group-s2), and [S3](#group-s3)

1. Initializes index tuples `czi`, `hc`, and `rg`
2. Opens [lefcfs](#file-21) and [dshares](#file-26)
3. Checks if local variable IB is 1, performs the following if so:
   1. Reads a line from [Record Rapid Processing File](#file-2) into [AC](#group-ac)
   2. Sets remaining record count to the total records for the current state and FC
   3. Writes to console and [Checkout File](#file-3) the state, fc, and records count.
   4. Sets `prst` to the current state through a spurious that may have been
      necessary in previous iterations of NAPCOM, but probably isn't in NAPCOM 2008.
   5. Checks if the traffic differentiation flag is set in [CN](#group-cn) and performs
      [VMT information formatting](#vmt-information-formatting) for the current state and FC
   6. Performs [vehicle miles matrix correction](#vmcr)
   7. Loads [P2](#group-p2) with values from [SD](#group-sd) for current state
4. Takes `n` as the max of remaining records to process and 1000
5. Adds `n` to total records `grec`
6. Reads `n` rows of data from the [binary section data file](#file-2) and stores it in [S2](#group-s2)
7. Enters a loop for `range(n)`:
   1. Performs modulus with [CN.nskp](#group-cn) to see if the record
      should be skipped.
   2. Imports data from [S2](#group-s2) into usable [S3](#group-s3) members.
   3. Evaluates whether state, fc, and pavement type are part of the study selected.
      If not, skips the entry by continuing the loop from (7)
   4. Imports more data from [S2](#group-s2) into usable [S3](#group-s3) members.
   5. Adjusts traffic for direction and lane distribution
   6. For pavement type 1, calls [Rho Beta](#rho-beta) function with argument (key of loop `i`)
   7. For pavement type not 1, calls [Rho-Beta Jointed Concrete Pavements](#rho-beta-jointed-concrete-pavements)
   8. Calls [Pavement Age Find](#psr-predictive-pavement-age-find)
   9. Totals accumulated Traffic before the first year of analysis based on pavement age
   10. If print options from [CN](#group-cn) are set to true, print record number,
       pavement type, and age to [Checkout](#file-3) and console.
   11. Calls [Pavement Condition Calculator](#pavement-condition-calculator) and passes `yr`
   12. Calculate pavement condition
   13. If the pavement condition is sufficiently bad (<= [CN.trgr](#group-cn)):
       1. Accumulate the lane miles of failed pavement by year
       2. Call [Divvy Costs](#divvy-costs)
       3. Reset accumulated traffic, age, and `ctraf`
   14. Add 1 to year and go back to (7.11) if the analysis period or 50 years has not been reached.
   15. Accumulate lane miles and add it to the last index of "bad pavement" matrix for running total
   16. Print the record number if it is evenly divisible by 30.
8. If the remaining number of records is more than none, increase the index boundary.
   If not, set the index boundary back to 1 and call [Cell Print](#cell-print) if the
   year is greater than 0.
9. Go back to (3) - _here seems to be an issue... The function says to go back to an earlier point in
   its body, but unless an error is encountered or PROCGRP exits (a condition I have not ovserved),
   it would theoretically run infinitely if it did not first encounter a mathematical crash_
10. Return to [Main](#main-function)

<p id="vmt-information-formatting">VMT Information Formatting Segment</p>

```fortran
         If (trdif .eq. 1) then
*
            sadt    = tvm(1,fc,st)
            afac    = 1.d6 * tvm(1,fc,st) / (hpadt * tm * 365.24)
            tfac(1) = tvm(2,fc,st) / hpsu
            tfac(2) = tvm(3,fc,st) / hpcmb
*
            Write (*,'(a21,f11.0,2(f8.1,a1))') '      HCAS AADT, Trk:',
     +                              1.d6 * tvm(1,fc,st) / (tm * 365.24),
     +                                     (100.*tvm(i,fc,st),'%',i=2,3)
            Write (*,'(a21,f11.0,2(f8.1,a1))') '      HPMS Sec Data: ',
     +                            hpadt, 100.*hpsu, '%', 100.*hpcmb, '%'
            Write (*,'(a21,f11.3,2f9.3)') '      Multipliers:   ', afac,
     +                                                  tfac(1), tfac(2)
            Write (3,'(a21,f11.0,2(f8.1,a1))') '      HCAS AADT, Trk:',
     +                              1.d6 * tvm(1,fc,st) / (tm * 365.24),
     +                                     (100.*tvm(i,fc,st),'%',i=2,3)
            Write (3,'(a21,f11.0,2(f8.1,a1))') '      HPMS Sec Data: ',
     +                            hpadt, 100.*hpsu, '%', 100.*hpcmb, '%'
            Write (3,'(a21,f11.3,2f9.3)') '      Multipliers:   ', afac,
     +                                                  tfac(1), tfac(2)
         Else
            afac    = 1.
            tfac(1) = 1.
            tfac(2) = 1.
         Endif
```

<p id="vmcr">Vehicle Miles Matrix Correction Algorithm</p>

```fortran
         Do 2 vc = 1, 24
           mult = tvm(typ(vc)+1,fc,st)
           If (typ(vc) .eq. 0) mult = 1.
         Do 2 wg = 1, 40
    2    vmcr(wg,vc,fc) = vmcr(wg,vc,fc) + tvm(1,fc,st) * mult *
     +                         vmt(vc,fc,st) * wtds(wg,hc(fc),rg(st),vc)
```

---

## Rho Beta

NAPCOM 2008 Name: `RHOBET`

Uses variables from common groups [AB](#group-ab), [CN](#group-cn), [DP](#group-dp),
[SC](#group-sc), [SH](#group-sh), [SD](#group-sd), [P2](#group-p2), [VM](#group-vm),
[TB](#group-tb), [NT](#group-nt), and [EE](#group-ee)

Computes the Rho and Beta values for damage functions of flexible
pavements using 'BRE' equations for the first year for each section.

1. Imports variables from [SD](#group-sd) to [P2](#group-p2)
2. Sets dummy ratios
3. Sets rhos and betas from [AB](#group-ab)
4. If interpolation flag is set in [CN](#group-cn),
   loop through dimensions of max loads matrix and calculate
   tire pressure [^1]. Set the corresponding rutting
   matrix element by calling [Rutting Calculation](#rutting-calculation)
5. Calculate [EE](#group-ee) elements using [EE Calculation sequence](#ee-calculation)
   for later use by [Fatigue Cracking Model](#eres-fatigue-cracking-model)
6. Zero out share arrays (using sufficiently small values where necessary to prevent log and division of zero)
   1. For every weight distribution sufficiently large (greater than 1.0E-15),
      use [VM](#group-vm) tables to pull Axle Load, Axle Type, and `vshare`.
   2. Determine Rho and Beta for 18-kip axle using [Rho Beta Formula](#rho-beta-formula).
   3. Calculate `lefs` as the ratio of `vshare` to the output
      of [Fatigue Cracking Function](#eres-fatigue-cracking-model)
   4. Calculate Tire Pressure (even though it is not used), calculate `lefs`
      as the ratio of `vshare` to the output of [Rutting Function](#ERES-Rutting-Model)
   5. Calculate skid resistance by accumulating total weight of axles
   6. Populate thermal cracking field with `vshare` divided by number of axles in the specified visual class.
   7. Aggregate this individually in axle shares and wholly for each visual class and weight group in
      share matrix. Also, add the cost to the total array for all weight groups and visual classes
   8. Take the share matrix for each weight group and visual class and divide it by the total array to get
      a matrix of the percentages of costs each weight group and visual class is responsible for
      creating.
   9. Also, take the axle-shares matrix and perform the same percentage analysis for each axle type,
      weight group, and visual class.
7. Compute reduced form of environmental damage equations and loss of serviceability for expansive clay
   1. Generate a random number using pseudo random function
   2. Compare the percent of clays to the random number. If lesser, return a zero value. If greater:
      1. Improt the state's information for clays from [SD](#group-sd) into [P2](#group-p2)
      2. Return the output described in [Clays Formula](#clays-formula)
8. Import thermal cracking information for current state from [SD](#group-sd) into [P2](#group-p2)
9. Transform the variables of the thermal cracking model using [Thermal Cracking Transformations](#thermal-transformation)
10. Return to [Processing Group](#processing-group-function)

[^1]: Tire pressure is not used for any calculation or output in NAPCOM 2008

<p id="ee-precalculation">EE Calculation Sequence</p>

```fortran
      EE3 = 2.79d-4 - 1.032d-7 * easc - 4.681d-5 * thck
     +     -7.87d-7 * ebse + 8.39d-8 * ebse * thck
     +     +1.03d-8 * thck * easc + 2.057d-6 * thck * thck
      If (ee3 .lt. 1.d-12) ee3 = 1.d-12
      EE12 = 7.53d-4 - 2.87d-7 * easc -8.643d-5 * thck
     +     - 4.415d-6 * ebse - 3.405d-6 * tbse
     +     - 1.350d-6 * esub + 1.485d-8 * easc * thck
     +     + 1.535d-9 * easc * ebse + 2.6d-7 * thck * ebse
     +     + 3.140d-6 * thck * thck + 7.7728d-9 * ebse * ebse
      If (ee12 .lt. 1.d-11) ee12 = 1.d-11
      EE30 = 1.126d-3 - 3.263d-7 * easc - 7.256d-5 * thck
     +     - 6.6103d-6 *ebse - 2.441d-5 * tbse
     +     - 8.461d-6 * esub + 2.9d-9 * easc * ebse
     +     + 4.866d-7 * thck * ebse +1.6221d-6 * thck * tbse
     +     + 6.444d-7 * esub * tbse
      If (ee30 .lt. 1.d-10) ee30 = 1.d-10
      EE6  = 2.67d-4 - 7.15d-8 * easc - 4.146d-5 * thck
     +     - 1.065d-6 * ebse - 7.48d-7 * tbse
     +     + 2.114d-6 * thck * thck + 4.8317d-10 * ebse * easc
     +     + 8.392d-8 * thck * ebse
      If (ee6 .lt. 5.d-12) ee6 = 5.d-12
      EE24 = 6.6d-4 - 1.95d-7 * easc - 8.80995d-5 * thck
     +     - 3.375d-6 * ebse - 2.935d-6 * tbse
     +     + 3.376d-6 * thck * thck + 1.4894d-9 * ebse * easc
     +     + 2.610d-7 * thck * ebse
      If (ee24 .lt. 5.d-11) ee24 = 5.d-11
      EE60 = 1.084d-3 - 3.19d-7 * easc -1.01d-4 * thck
     +     - 6.443d-6 * ebse - 1.6634d-5 * tbse
     +     + 2.2229d-6 *thck * thck +2.758d-9 * ebse * easc
     +     + 4.880d-7 * thck * ebse +1.599d-6 * tbse * thck
      If (ee60 .lt. 5.d-10) ee60 = 5.d-10
```

<p id="rho-beta-formula">Rho Beta Formula for 18-Kip Axle</p>
_Note: the formula for calculating Beta is commented out and a default value of 1 is used instead_

```fortran
            If (axtp(ax,vc) .eq. 3) then
               Lx = Lx / 1.5
               L2 = 2
            Endif
            rh18 = ab(z,1,1,d) + ab(z,2,1,d) * (Lx+L2) ** dum1
     +                      * L2 ** dum2 * SUBM ** ab(z,9,1,d)
     +                      * STRN ** ab(z,10,1,d) * thk ** ab(z,11,1,d)
*           be18 = ab(z,1,2,d) + ab(z,2,2,d) * (Lx+L2) ** dum3
*     +                     * L2 ** dum4 * subm ** ab(z,9,2,d)
*     +                     * STRN ** ab(z,10,2,d) * thk ** ab(z,11,2,d)
            be18 = 1.
            lefs = rhz(d) / rh18
            If (axtp(ax,vc) .eq. 3) lefs = lefs * 1.5
            lefs = lefs * vshr
```

<p id="clays-formula">Clays Formula</p>

$$
\dfrac{0.87(exsp)^0.13(clay)^3.05}{(dpth)^0.20(cexc)^1.22(acpi)^1.31(rnge)^1.22}
$$

```fortran
0.087 * (EXSP ** 0.13) * (CLAY ** 3.05) /
     +       ((DPTH**0.20) * (CEXC**1.22) * (ACPI**1.31) * (RNGE**1.22))
```

<p id="thermal-transformation">Thermal Transformation Formula</p>

```fortran
      PENI      = 0.25 * (PENI + 2.)
      RBSP      = RBSP / 125.6
      DOAL      = DOAL / 8.
      AASR      = AASR / 240.
      TPMM      = (TPMM + 20.) / 55.7
*
      CDMG = (PENI**0.257) * (RBSP**0.122) * 0.519 * (VCOA**24.5) *
     +                    (AASR**1.970) / ((DOAL**0.410) * (TPMM**7.43))
```

---

## Rho Beta Jointed Concrete Pavements

NAPCOM 2008 Name: `RBJCP`

Computes rho and beta values for jointed plain concrete pavements and
for jointed reinforced concrete pavements.

Uses variables from common groups [RP](#group-rp), [DP](#group-dp),
[SC](#group-sc), [SH](#group-sh), [SD](#group-sd), [P2](#group-p2), [VM](#group-vm),
[TB](#group-tb), and [P3](#group-nt)

1. Assigns from [SD](#group-sd) to [P2](#group-p2) and [P3](#group-p3) according to
   pavement type and current state.
2. For Pavement Type 2:
   1. Assign beta to 0.6948[^2]
   2. Calculate [loss of serviceability distress](#lsd-pv2)
   3. Calculate loss of skid resistance distress[^3]
   4. Calculate [cracking distress](#cracking-pv2)
3. For pavement types greater than 2:
   1. Assign beta to 0.87568[^2]
   2. Calculate [Loss of Serviceability distress](#lsd-pvn)
   3. Calculate Loss of Skid Resistance[^3]
   4. Calculate [Cracking distress](#cracking-pvn)
4. For each vc and weight group:
   1. Apply PSR model from PVDM82 using [ESAL Function](#pvdm82-esal-model)
   2. Apply Faulting model from ERES using [Faulting Function](#eres-faulting-model)
   3. Calculate skid resistance by accumulating total weight of axles
   4. Calculate slab cracking by taking the PSR to the power of the beta[^4]
   5. Calculate spalling for mixed load/non-load as a proportion of PSR loss
   6. Calculate swelling and depression for non-load as the sum of damage divided by axles for
      the visual class.
5. Transform axle shares and shares matrices by turning them from absolute cost values to
   ratios of the calculated costs to the total costs.
6. Return to [Processing Group](#processing-group-function)

[^2]: This part of code is commented out in NAPCOM 2008

<p id="lsd-pv2">Distress Type: Loss of Serviceability (pavement type 2)</p>

```fortran
         RHOR   = 1.333 * STYP - 0.009024 * FRZI
     +          + BTYP * (1.156 * SLBT - 6.966)
     +          + JLTS * (0.6556 * SLBT + 1.763) + 0.803
         rhz(1) = DEXP(RHOR) * 1.d6
         bez(1) = 0.0006076 * FRZI + BTYP * (-0.01435 * SLBT - 0.0683)
     +          + JLTS * (-0.09997 * SLBT + 0.7107) + 0.544
         If (bez(1) .lt. 0.544) bez(1) = 0.544
*         If (rhz(1) .gt. 1.d17) Write (3,*)
*      +      ' rhz(1), btyp, jlts, slbt: ', rhz(1), btyp, jlts, slbt
*
```

[^3]: No formula is used for loss of skid resistance. Instead, constant Rho of 1.11E9 and beta of 0.299 are used.

<p id="cracking-pv2">Distress Type: Cracking</p>

```fortran
         RHOR   = JLTS * (4.872 + 0.0435 * (SLBT-7.)**3)
     +          + BTYP * (0.0535 * SLBT * SLBT - 0.2745 * SLBT)
     +          +1.698 * STYP - 0.105 * TDIF + 2.386
         rhz(4) =  DEXP(RHOR) * 1.d6
         bez(4) = 1.510 + 0.16 * BTYP
*         If (rhz(4) .gt. 1.d17) Write (3,*) ' rhz(4), jlts, slbt: ',
*     +                                         rhz(4), jlts, slbt
*
```

<p id="lsd-pvn">Distress Type: Loss of Serviceability for Pavement Type > 2</p>

```fortran
         RHOR   = 0.4593 * SLBT - 0.01167 * THMI + 0.6758 * BTYP - 1.709
         rhz(1) = DEXP(RHOR) * 1.d6
         bez(1) = 7.656 / jtsp + 0.04152 * TOBL + 0.43516
*         If (rhz(1) .gt. 1.d17) Write (3,*)
*      +       ' rhz(1), btyp, thmi, slbt:', rhz(1), btyp, thmi, slbt
*
```

<p id="cracking-pvn">Distress Type: Cracking for Pavement Type >2</p>

```fortran
         RHOR   = 79.51 / AARF - 0.5949 * SLBT + 0.7 * DRNT
     1          - 0.0011546 * FRZI + 0.550745 * BTYP + 2.805
     2          + 0.053188 * SLBT * SLBT
         rhz(4) =  DEXP(RHOR) * 1.d6
         bez(4) = -0.003513 * THMI + 1.324
*         If (rhz(4) .gt. 1.d17) Write (3,*)
*     +        ' rhz(4), aarf, slbt, drnt:', rhz(4), aarf, slbt, drnt
```

[^4]: This isn't the right way to do this. NAPCOM 2008 admits that it's just a temporary fix.

---

## PSR Predictive Pavement Age Find

NAPCOM 2008 Name: `AGEFND`

Estimates the pavement age based on condition and traffic levels.

Uses variables from common groups [SC](#group-sc) and [DP](#group-dp)

1. Clamps Rho to be the maximum between the calculated rho and the rho of the total.
2. Until the Pavement Serviceability Rating is less than the pavement condition or the
   age is greater than or equal to 25 years, perform the following:
   1. Increment age
   2. Apply average combined traffic summation `totl(1) * 365.24 * (1.+gf) ** (1-age)`
   3. Apply gnom calculation: average combined traffic T: $$(T / rho)^(\dfrac{beta}{T / rho})$$
   4. Adjust PSR as a factor of gnom where gnom = g in `4.5-2.0g`
3. Return to [Rho Beta Function](#rho-beta) or [Rho Beta for Jointed Concrete Pavements](#rho-beta-jointed-concrete-pavements)

---

## Divvy Costs

NAPCOM 2008 Name: `DIVVY`

Compiles vehicle class and weight group shares after each section fails.
[Cell Print](#cell-print) and [Result Print](#result-print) write state results
and final results by functional class. [Zero Shares](#zero-shares) initializes
share arrays and zeroes them as necessary.

Uses variables from common groups [CN](#group-cn), [DP](#group-dp), [SC](#group-sc),
[S3](#group-s3), [RC](#group-rc), [SH](#group-sh), [VM](#group-vm), [HP](#group-hp), and [TB](#group-tb)

1. Takes output matrices as adjusted sums of [HP](#group-hp) Share Arrays across visual class, weight group, axle type, cost model figure, and pavement type.
2. Returns to [Processing Group](#processing-group-function)

### Cell Print

NAPCOM 2008 Name: `CELPRNT`

For each pavement, distress type, and axle type, apply the least-squares method
to fit a straight line to the observed logarithms of share per mile. This function
performs linear regression analysis to predict future cost and damage levels.

1. For each pavement type and damage type:
   1. For each visual class, weight, and axle combination:
      1. Take Y as the log10 of the ratio between axle share for the specivied fields and the
         vehicle miles traveled weight (meaning less weight for more vehicle miles)
      2. Calculate other regression variables in aggregate
   2. Perform linear regression with the aggregated variables
   3. If the slope is increasing, write the result of the regression to [File 21](#file-21)
2. Write attributable shares and shares by damage type to [checkout file](#file-3) and screen
3. Write share arrays by pavement type to [File 26](#file-26)
4. Reset the share arrays to zero (or a suitably low number such as 1.0E-30)
5. Return to [Processing Group](#processing-group-function)

### Result Print

NAPCOM 2008 Name: `RSLTPRN`

Writes the overall results for the run after processing all sections.

1. Open relevant files
2. Write the headers to them so they can be understood for the data they contain
3. for each FC:
   1. Write the FC number to the [Attributable Shares File](#file-25)
   2. Total the distress from the share arrays
   3. If the total is sufficiently nonzero, turn the attributable shares into cost by dividing each attributable shares array element by the total from (3.2)
   4. If the the total is zero, write zero to the field.
   5. For each weight group and visual class:
      1. Take the ratio of `vmtc` to its corresponding `vmcr`
      2. Add the `almsh` values' sums for each corresponding element of `shrd` and `shrs`
      3. take the sum to be the sum of the whole `almsh` table
4. Write [jakeout tables](#file-23)
5. Write [rshares tables](#file-7)
6. Write [attributable shares tables](#file-9.2)
7. Write [Failed Lane Miles by Pavment Type and FC](#file-22)
8. Write Load Equivalency Factors (LEFs) by damage type, functional class, and pavement type to [LEFs File](#file-25.2)
9. Return to [NAPCOM Main](#main-function)

### Zero Shares

NAPCOM 2008 Name: `ZERSHR`

Set all share array elements to zero or a sufficiently small nonzero value to simulate infinity as the
result of a division by zero and return to [Processing Group](#processing-group-function)

---

## ERES Fatigue Cracking Model

NAPCOM 2008 Name: `FCRACKN`

Calculates the number of allowable load applications to cause 20% fatigue cracking.

It is the analyst's opinion that it would be better for a civil engineer
to interpret this than a software analyst: the models are highly mathematical
but the logic is simple. Returns to [Rho Beta](#rho-beta)

```fortran
      Integer*2 L2
      Integer*4 sec
      Real*8    Lx, M, C, EP, Vb, Vv
*
      Vb = 11.
      Vv =  5.
*
      If (L2 .eq. 1) then
         If (Lx .lt. 3.) then
            EP  = (Lx/3.) * EE3
         Elseif (Lx. ge. 3. .and. Lx .lt. 12.) then
            EP = EE3  + ((EE12 - EE3) / 1.7320) * (SQRT(Lx) - SQRT(3.))
         Elseif (Lx .ge. 12. .and. Lx .lt. 30.) then
            EP = EE12 + ((EE30 -EE12) / 2.0131) * (SQRT(Lx) - SQRT(12.))
         elseif (Lx .ge. 30) then
            EP = (Lx/30.) * EE30
         Endif
      Elseif (L2 .ge. 2) then
         Lx = (2.*Lx) / L2
         If (Lx .lt. 6) then
            EP = (Lx/6.) * EE6
         Elseif (Lx.ge.6. .and. Lx.lt.24.) then
            EP = EE6 +  ((EE24 - EE6)  /2.44950) * (SQRT(Lx) -SQRT(6.))
         Elseif (Lx.ge.24. .and. Lx.lt.60.) then
            EP = EE24 + ((EE60 - EE24) /2.84698) * (SQRT(Lx) -SQRT(24.))
         Elseif (Lx.ge.60) then
            EP = (Lx/60.) * EE60
         Endif
      Else
          Write(*,*) 'Wrong axle type!'
          Stop
      Endif
*
C-----Calculate N
      M = 4.84 * (Vb/(Vb+Vv) - 0.6875)
      C = 10.0**M
      If (EP .lt. 1.d-13) then
         Write (3,*) 'FCRACKN: Lx, L2, EP:', Lx, L2, EP
         EP = Dabs(EP)
      Endif
      FCRACKN = 18.4 * C * 4.325d-3 * EP**(-3.291) * 3000**(-0.854)
      If (L2 .eq. 3) then
         FCRACKN = 0.6667 * FCRACKN
         Lx = Lx * 1.5
      Endif
	If (sec .eq. 1 .and. prnt .eq. 1)
     +    Write (3,'(f8.3,i5,f20.12,f20.5)') Lx, L2, ep, log10(fcrackn)
*
        Return
        End

```

---

## ERES Rutting Model

NAPCOM 2008 Name: `RUTN`

Calculate the number of axle passes to failure for a given axle

It is the analyst's opinion that it would be better for a civil engineer
to interpret this than a software analyst: the models are highly mathematical
but the logic is simple. Returns to [Rho Beta](#rho-beta)

```fortran
      a1 = 0.6
      a2 = 0.7
      a3 = 0.7
      n = 1.d6
      page = 18.
*     (assumed age at time of failure)
*
      If (sc .eq. 1) then
         i = 0
    1    i = i + 1
         If (Lx .gt. mlds(i,L2)) then
            If (i .lt. 24) Go to 1
            n = nrut(24,L2)
         Else
            If (i .eq. 1) n = nrut(1,L2)
            If (i .ge. 2) n = 10.** (dlog10(nrut(i-1,L2)) +
     +                       (dlog10(nrut(i,L2))-dlog10(nrut(i-1,L2))) *
     +                               (dlog10(Lx)-dlog10(mlds(i-1,L2))) /
     +                        (dlog10(mlds(i,L2))-dlog10(mlds(i-1,L2))))
         Endif
      Else
    2    rutd = 0.286 * page **0.13 *
     +        (thck * (n * CSac(Lx,L2)   **(1./(1.-a1))) **(1.-a1)
     +      +  tbse * (n * CSbase(Lx,L2) **(1./(1.-a2))) **(1.-a2)
     +      +  12.0 * (n * CSsubg(Lx,L2) **(1./(1.-a3))) **(1.-a3))
         dmge = rutd / cldg(3)
         If (dmge .le. 1.d-6) then
            n = n * 1.d6
         Elseif (dmge .gt. 1.0001 .or. dmge .lt. 0.999) then
            n = n / dmge
            Go to 2
         Endif
      Endif
	If (sec .eq. 1 .and. prnt .eq. 1)
     +    Write (3,'(f8.3,i5,f20.0)') Lx, L2, n
      rutn = n

```

---

## PVDM82 ESAL Model

NAPCOM 2008 Name: `ESAL`

It is the analyst's opinion that it would be better for a civil engineer
to interpret this than a software analyst: the models are highly mathematical
but the logic is simple. Returns to [Rho Beta for Jointed Concrete Pavements](#rho-beta-jointed-concrete-pavements)

```fortran
      Data tpsi /2.5/
*
      If (pavt .eq. 1) then
         lgt = (4.2 - tpsi) / 2.7
         Gt = Dlog10(lgt)
         B18 = 0.4 + 1093.602/ (thk+1) **5.19
         Beta = .4 + .081*(Lx+L2)**3.23 / ((thk+1) **5.19 * L2 **3.23)
         Esal = ((Lx+L2)/19)**4.79 * lgt**(1./B18-1./Beta) / L2 **4.33
      Else
         lgt = (4.5 - tpsi) / 3.0
         Gt = Dlog10(lgt)
         B18 = 1 + 1.624E+7 / (thk+1) **8.46
         Beta = 1. + 3.63 * (Lx+L2)**5.20 / ((thk+1)**8.46 * L2**3.52)
         Esal = ((Lx+L2)/ 19) **4.62 * lgt **(1/B18-1/Beta) / L2 **3.28
      Endif
```

---

## ERES Faulting Model

NAPCOM 2008 Name: `RFAULTN`

It is the analyst's opinion that it would be better for a civil engineer
to interpret this than a software analyst: the models are highly mathematical
but the logic is simple. Returns to [Rho Beta for Jointed Concrete Pavements](#rho-beta-jointed-concrete-pavements)

Calculate the number of applications of a particular load to failure.

```fortran
      thk = slbt
      axsp = 48.
      Lx = Lx * 1000.
*
C-----Calculate l (radius of relative stiffness)
      l = (Ecnc * 1000. * (thk**3) / (12 * (1-mu) * ksub)) ** (0.25)
      lsq = l * l
*
C-----Get the wfe0 and wfe36
      If (L2.eq.1) then
         wfe0 = (0.0000864 *lsq +0.002824 *l +0.29530) *Lx/(ksub *lsq)
         wfe36 =(0.0000648 *lsq +0.003934 *l -0.02548) *Lx/(ksub *lsq)
      Elseif (L2.eq.2) then
         wfe0 = (1.0023 - 0.0337002 * axsp + 0.000308639 * axsp * axsp
     +        - 0.043436 * l + 0.00178717 * axsp * l
     +        - 0.0000168611 * axsp * axsp * l + 0.000796801 * lsq
     +        - 0.0000265334 * axsp * lsq
     +        + 2.41667d-07 * axsp * axsp * lsq) * Lx/(ksub *lsq)
         wfe36 = (-0.142828 + 0.00360675 *axsp -0.0000174028 *axsp *axsp
     +         + 0.00909779 * l - 0.000251908 *axsp *l
     +         + 0.000001473 * axsp *axsp *l - 0.0001004 * lsq
     +         + 0.000006225 * axsp *lsq
     +         - 5.13889d-08 * axsp * axsp * lsq) * Lx/(ksub *lsq)
      Elseif (L2.eq.3) then
         wfe0 = (0.43246 - 0.0138288 * axsp + 0.000135903 *axsp *axsp
     +        - 0.01548 * l + 0.000634833 * axsp *l
     +        - 6.33333d-06 * axsp *axsp *l + 0.000649091 * lsq
     +        - 1.96378d-05 * axsp *lsq
     +        + 1.708d-07 *axsp *axsp *lsq) * Lx/(ksub *lsq)
         wfe36 = (-0.572713 + 0.0215153 * axsp -0.000187292 *axsp *axsp
     +         + 0.0313199 * l - 0.00122083 * axsp * l
     +         + 0.0000109722 *axsp *axsp *l - 0.000423601 * lsq
     +         + 0.0000190917 * axsp * lsq
     +         - 1.79167d-07 * axsp * axsp * lsq) * Lx/(ksub *lsq)
      Else
         Write(*,*) 'The axle type is wrong!'
         Stop
      Endif
*
C-----Calculate the AGG/kl and LTE0, LTE36
      aggkl = 2. * exp(1 + 0.2 * dodb**2 -0.17 *jtsp /l)
      LTE0  = 0.01 / (0.01 + 0.012 * aggkl**(-0.849))
      LTE36 = 0.01 / (0.01 + 0.003483 * aggkl**(-1.13677))
*
C-----Calculate wul0,wul36,wl0,wl36
      wul0  = wfe0  * LTE0  / (1. + LTE0)
      wl0   = wul0  / LTE0
      wul36 = wfe36 * LTE36 / (1. + LTE36)
      wl36  = wul36 / LTE36
*-----Calculate wul and wl.(a load placed 12 in from the corner)
C     These are calculated by linear interpolation from the results
C     obtained for loads placed at the corner and at 36 in from the corner.
      wul   = wul0  + (wul36 - wul0) / 3.
      wl    = wl0   + (wl36  - wl0) / 3.
*-----Calculate DE, RFAULTN
      DE    = ksub / 2. * (wl + wul) * (wl - wul)
*
      If (de .le. 0.002) de = 0.002 + 1.d-12
      RFAULTN = 10 ** (6.27 - 1.6 * Dlog10(DE - 0.002))
*

```

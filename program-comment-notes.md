Comment notes
================

- Line 3362 contains a non-functional current model calculation
- Verbose mode settings starting on line 545
- Conditional regarding pavement type "2" on lines 638-660
- Additional data output possibility on line 800
- Beta value is subbed out as 1 on line 861, where it is commented out that it should be calculated on line 858.
- Additional data output possibility on line 923 regarding existence of expansive clay
- Debug code commented out on line 1064
- Debug code commented out on line 1079
- Debug code commented out on line 1091
- Debug code commented out on line 1107
- Additional data output possibility on line 1479
- Conditional regarding hpstat variable on line 2438-2444. According to comment, "This section of code converts IRI to PSR when the state provided PSR is inadequate." (note for Dr. Bryce, what are IRI and PSR?)
- Error handling procedure on line 2686-2693
- Potential update to model on lines 3365-3374 (problematic according to comments)
- additional VMT readings on lines 3418 and 3419, seems to be rewritten on 3417 with a different format
- Additional axle weights reading on line 3426, seems to be rewritten on 3427 with a different format

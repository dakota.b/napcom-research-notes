# NAPCOM Revival -- Extended Goals

1. Reduce runtime through parallelization of processing. As far as the developer's research covers, no parallelization attempt was made.
   This might be due to NAPCOM's intended use on older systems which often utilized single-core hardware implementations would suffer
   overhead from parallelization procedures and the code would be further complicated by this approach. It now makes more sense to
   parallelize processing of the data, which can reduce task runtime by as much as or more than 94%.
2. Reduce runtime by removing or adjusting memory constraints and minimizing disk write actions. The current 2008 iteration of NAPCOM
   writes files continuously during operation, resulting in a dramatic performance loss on even high-performance NVMe solid state
   implementations of hardware. The proposal is to use superior methods of storing information for processing such as vast 8+GB
   RAM reserves available on modern systems. Further research is needed on the performance impact this could have, but speed could
   be improved by as much as or over 99% by implementing this approach.
3. Create more manageable code by making Object-oriented class structures for storing different types of data. Currently, data is
   stored in many discrete primitive variable types and processed by functions which redeclare the variables they use. This leads
   to increased transfer rates and reduced maintainability of code. Additionally, in order to make functions perform slightly different
   procedures or data operations, intricate conditional trees or re-implementations of those functions must be made and named
   something different, even if the function performed is essentially the same. Object-oriented class structure would help to
   further modularize the code and help potential developers with less code experience make meaningful changes to parts of the
   program without unintentionally impacting other parts. This means an easier time with implementing complex equations and processing
   procedures.

## Small Improvements

1. Make control.py automatically detect the HPMS dataset format
